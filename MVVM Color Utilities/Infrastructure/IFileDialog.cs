﻿namespace MVVM_Color_Utilities.Infrastructure;

//Dialog
public interface IFileDialog
{
    bool OpenImageDialogBox(out string filePath);

    bool SaveImageDialogBox(out string filePath);
}
