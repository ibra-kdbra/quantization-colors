# MVVM-Color-Utilities

## Features
- Image quantizer supporting Median Cut, Octree, Popularity and Naive quantizers
- Image analyzer with configurable quantizer and color count selection
- Color browser
